﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using minsaludApi.Data;
using minsaludApi.Models;

namespace minsaludApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeguimientosController : ControllerBase
    {
        private readonly minsaludApiContext _context;

        public SeguimientosController(minsaludApiContext context)
        {
            _context = context;
        }

        

        // GET: api/Seguimientos
        [HttpGet("{idPersona}")]
        public async Task<ActionResult<IEnumerable<Seguimiento>>> GetSeguimiento(int idPersona)
        {
            if (_context.Seguimiento == null)
            {
                return NotFound();
            }
            return await _context.Seguimiento.Where(s=>s.IdPersona == idPersona).ToListAsync();
        }

        // GET: api/Seguimientos/5
        [HttpGet]
        public async Task<ActionResult<Seguimiento>> GetSeguimiento(int id, int idPersona)
        {
            if (_context.Seguimiento == null)
            {
                return NotFound();
            }
            var seguimiento = await _context.Seguimiento.FindAsync(id);

            if (seguimiento == null)
            {
                return NotFound();
            }

            return seguimiento;
        }

        // PUT: api/Seguimientos/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSeguimiento(int id, Seguimiento seguimiento)
        {
            if (id != seguimiento.Id)
            {
                return BadRequest();
            }

            _context.Entry(seguimiento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SeguimientoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Seguimientos
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Seguimiento>> PostSeguimiento(Seguimiento seguimiento)
        {
            if (_context.Seguimiento == null)
            {
                return Problem("Entity set 'minsaludApiContext.Seguimiento'  is null.");
            }
            _context.Seguimiento.Add(seguimiento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSeguimiento", new { id = seguimiento.Id }, seguimiento);
        }

        // DELETE: api/Seguimientos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSeguimiento(int id)
        {
            if (_context.Seguimiento == null)
            {
                return NotFound();
            }
            var seguimiento = await _context.Seguimiento.FindAsync(id);
            if (seguimiento == null)
            {
                return NotFound();
            }

            _context.Seguimiento.Remove(seguimiento);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SeguimientoExists(int id)
        {
            return (_context.Seguimiento?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
