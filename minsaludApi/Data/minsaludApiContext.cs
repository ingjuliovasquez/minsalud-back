﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using minsaludApi.Models;

namespace minsaludApi.Data
{
    public class minsaludApiContext : DbContext
    {
        public minsaludApiContext (DbContextOptions<minsaludApiContext> options)
            : base(options)
        {
        }
     
        public DbSet<Persona> Persona { get; set; } = default!;
        public DbSet<Seguimiento> Seguimiento { get; set; } = default!;
        
    }
}
