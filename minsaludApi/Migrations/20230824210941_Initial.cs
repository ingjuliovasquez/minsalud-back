﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace minsaludApi.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Persona",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TipoIdentificacion = table.Column<string>(type: "TEXT", nullable: false),
                    NroIdentificacion = table.Column<string>(type: "TEXT", nullable: false),
                    PrimerNombre = table.Column<string>(type: "TEXT", nullable: true),
                    SegundoNombre = table.Column<string>(type: "TEXT", nullable: true),
                    PrimerApellido = table.Column<string>(type: "TEXT", nullable: false),
                    SegundoApellido = table.Column<string>(type: "TEXT", nullable: false),
                    Sexo = table.Column<string>(type: "TEXT", nullable: false),
                    FechaNacimiento = table.Column<DateTime>(type: "TEXT", nullable: false),
                    CodMpioResidencia = table.Column<string>(type: "TEXT", nullable: false),
                    CodAsegurador = table.Column<string>(type: "TEXT", nullable: false),
                    Created = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Updated = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persona", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Seguimiento",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IdPersona = table.Column<int>(type: "INTEGER", nullable: false),
                    EstadoVital = table.Column<int>(type: "INTEGER", nullable: false),
                    FechaDefuncion = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UbicacionDefuncion = table.Column<int>(type: "INTEGER", nullable: false),
                    CodLugarAtencion = table.Column<string>(type: "TEXT", nullable: false),
                    FechaAtencion = table.Column<DateTime>(type: "TEXT", nullable: false),
                    PesoKg = table.Column<decimal>(type: "TEXT", nullable: false),
                    TallaCm = table.Column<int>(type: "INTEGER", nullable: false),
                    CodClasificacionNutricional = table.Column<string>(type: "TEXT", nullable: false),
                    CodManejoActual = table.Column<string>(type: "TEXT", nullable: false),
                    DesManejo = table.Column<string>(type: "TEXT", nullable: false),
                    CodUbicacion = table.Column<string>(type: "TEXT", nullable: false),
                    DesUbicacion = table.Column<string>(type: "TEXT", nullable: false),
                    CodTratamiento = table.Column<string>(type: "TEXT", nullable: false),
                    TotalSobresFTLC = table.Column<int>(type: "INTEGER", nullable: false),
                    OtroTratamiento = table.Column<string>(type: "TEXT", nullable: false),
                    Created = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Updated = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seguimiento", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Seguimiento_Persona_IdPersona",
                        column: x => x.IdPersona,
                        principalTable: "Persona",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Seguimiento_IdPersona",
                table: "Seguimiento",
                column: "IdPersona");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Seguimiento");

            migrationBuilder.DropTable(
                name: "Persona");
        }
    }
}
