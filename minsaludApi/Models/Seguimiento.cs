﻿namespace minsaludApi.Models
{
    public enum EstadoVital { Vivo, Fallecido }
    public enum UbicacionDefuncion { IPS, Hogar }

    public class Seguimiento
    {
        public int Id { get; set; }
        public int IdPersona { get; set; }
        public EstadoVital EstadoVital { get; set; }
        public DateTime FechaDefuncion { get; set; }
        public UbicacionDefuncion UbicacionDefuncion { get; set; }
        public string CodLugarAtencion { get; set; }
        public DateTime FechaAtencion { get; set; }
        public decimal PesoKg { get; set; }
        public int TallaCm { get; set; }
        public string CodClasificacionNutricional { get; set; }
        public string CodManejoActual { get; set; }
        public string DesManejo { get; set; }
        public string CodUbicacion { get; set; }
        public string DesUbicacion { get; set; }
        public string CodTratamiento { get; set; }
        public int TotalSobresFTLC { get; set; }
        public string OtroTratamiento { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
