﻿namespace minsaludApi.Models
{
    public class Persona
    {
        public int Id { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NroIdentificacion { get; set; }
        public string? PrimerNombre { get; set; }
        public string? SegundoNombre { get; set;}
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string CodMpioResidencia { get; set; }
        public string CodAsegurador { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }

    }
}
